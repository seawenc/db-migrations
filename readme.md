# 数据库整库迁移工具
动机：
本项目源于公司对国产化要求，要求将所有数据库都迁移到oceanbase上来，但是oceanbase对低版本，postgres支持不太好，而且不支持无主键表，导致无法使用，才有此项目

用来做数据库跨类型，整库迁移（数据库迁移），包含结构迁移与数据迁移,支持：mysql,oracle,postgres,oceanbase数据库的相互迁移

##  问题记录
https://zhuanlan.zhihu.com/p/643604969
1.自增列
2.分区


由于oceanbase的mysql模式与mysql基本一致，oceanbase的实现与mysql完全一致

postgresql 迁移到mysql时 TEXT 需转为LONGTEXT

目标表字段名不能有 mysql 关键字


## oceanbase与mysql对比  
与 MySQL 兼容性对比： https://www.oceanbase.com/docs/common-oceanbase-database-cn-1000000000217920


```shell
docker run -ti -v ${PWD}/conf/datax:/jobs --name datax --rm seawenc/datax bash
```

需要将datax.py中的java路径改为本地全路径