CREATE TABLE test_postgresql_types (
    id SERIAL PRIMARY KEY,
    age smallint DEFAULT 18,
    text_col VARCHAR(50) DEFAULT 'Default Text',
    int_col INTEGER DEFAULT 0,
    float_col FLOAT DEFAULT 0.0,
    bool_col BOOLEAN DEFAULT FALSE,
    salary numeric(10, 2) DEFAULT 0.00,
    date_col DATE DEFAULT CURRENT_DATE,
    time_col TIME DEFAULT CURRENT_TIME,
    timestamp_col TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    json_col JSON DEFAULT '{}'::JSON,
    array_col INTEGER[] DEFAULT ARRAY[]::INTEGER[]
);

COMMENT ON COLUMN test_postgresql_types.text_col IS 'This is a text column';
COMMENT ON COLUMN test_postgresql_types.int_col IS 'This is an integer column';
COMMENT ON COLUMN test_postgresql_types.float_col IS 'This is a float column';
COMMENT ON COLUMN test_postgresql_types.bool_col IS 'This is a boolean column';
COMMENT ON COLUMN test_postgresql_types.date_col IS 'This is a date column';
COMMENT ON COLUMN test_postgresql_types.time_col IS 'This is a time column';
COMMENT ON COLUMN test_postgresql_types.timestamp_col IS 'This is a timestamp column';
COMMENT ON COLUMN test_postgresql_types.json_col IS 'This is a JSON column';
COMMENT ON COLUMN test_postgresql_types.array_col IS 'This is an integer array column';