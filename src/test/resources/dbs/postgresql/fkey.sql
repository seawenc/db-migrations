CREATE TABLE test_fkey_departments (
    department_id SERIAL PRIMARY KEY,
    department_name VARCHAR(100)
);

CREATE TABLE test_fkey_employees (
    employee_id SERIAL PRIMARY KEY,
    employee_name VARCHAR(100),
    department_id INT REFERENCES test_fkey_departments(department_id)
);