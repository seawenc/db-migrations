
CREATE TABLE test_postgres_index (
    id SERIAL PRIMARY KEY,
    btree_index_col INTEGER,
    hash_index_col INTEGER,
    gist_index_col BOX,
    gin_index_col TSVECTOR,
    spgist_index_col POINT
);

CREATE INDEX btree_index ON test_postgres_index USING BTREE (btree_index_col);
CREATE INDEX hash_index ON test_postgres_index USING HASH (hash_index_col);
CREATE INDEX gist_index ON test_postgres_index USING GIST (gist_index_col);
CREATE INDEX gin_index ON test_postgres_index USING GIN (gin_index_col);
CREATE INDEX spgist_index ON test_postgres_index USING SPGIST (spgist_index_col);

CREATE INDEX combined_index ON test_postgres_index USING GIN (gin_index_col, spgist_index_col);