CREATE TABLE test_mysql_index (
    id INT,
    name VARCHAR(50),
    age INT,
    email VARCHAR(100),
    created_at TIMESTAMP,
    PRIMARY KEY (id),
    INDEX idx_name (name),
    UNIQUE INDEX idx_email (email),
    INDEX idx_age_email (age, email),
    FULLTEXT INDEX idx_fulltext_name (name)
);