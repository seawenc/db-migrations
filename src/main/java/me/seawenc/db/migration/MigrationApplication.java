package me.seawenc.db.migration;

import java.util.Scanner;
import me.seawenc.db.migration.bean.ExecParameters;
import me.seawenc.db.migration.service.MigrationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MigrationApplication {
    private static Logger logger = LogManager.getLogger(MigrationApplication.class);
    public static void main(String[] args) throws Exception {
        // 封装参数
        ExecParameters parameters = ExecParameters.getInstance(args);
        MigrationService migrationService=new MigrationService(parameters);

        // 構建迁移数据库结构
        migrationService.migrationDbStructure();

        // 数据迁移检查
        boolean checkResult = migrationService.migrationDbDataBeforeCheck();


        assertContinue();

        // 数据迁移
        migrationService.migrationDbData();
        logger.info("数据库迁移完成");
    }

    private static void assertContinue() {
        logger.info("是否继续（Y:继续，其它：退出）:");
        if(!"Y".equalsIgnoreCase(new Scanner(System.in).next())){
            logger.info("收到退出指令，程序退出");
            System.exit(0);
        }
    }
}
