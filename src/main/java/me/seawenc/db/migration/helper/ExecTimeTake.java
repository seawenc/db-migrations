package me.seawenc.db.migration.helper;

/**
 * 用于耗时打印
 */
public class ExecTimeTake {

    /**
     * 记录开始时间
     */
    long costBegin;

    /**
     * 构造方法
     */
    public ExecTimeTake(){
        costBegin=System.currentTimeMillis();
    }

    /**
     * 获得耗时
     * @return
     */
    public long get(){
        long now = System.currentTimeMillis();
        long cost = now - costBegin;
        return cost;
    }

    public String getFormat(){
        return format(get());
    }

    public static String format(long cost){
        if(cost>1000*60*60){
            return (cost/(1000*60))+"分钟";
        }
        if(cost>1000*60){
            return (cost/1000)+"秒";
        }
        return cost+"ms";
    }
}
