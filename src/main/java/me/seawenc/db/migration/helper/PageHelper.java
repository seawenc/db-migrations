package me.seawenc.db.migration.helper;

import java.util.List;
import java.util.function.Consumer;

/**
 * 翻页对象的类型
 * @param <T>
 */
public class PageHelper<T> {

    /**
     * 执行翻页
     * @param pageSize 每页条数
     * @param allRecord 所有记录
     * @param command 翻页数据处理
     * @throws Exception
     */
    public static <T> void start(Integer pageSize, List<T> allRecord, Consumer<List<T>> command) throws Exception {
        int totalPage = allRecord.size() / pageSize + (allRecord.size() % pageSize == 0 ? 0 : 1);
        for(int i = 1; i <= totalPage; i++){
            List<T> currentPage = allRecord.subList(pageSize * (i-1), (i == totalPage ? allRecord.size() : pageSize * i));
            if(Optionalx.isPresent(currentPage)){
                command.accept(currentPage);
            }
        }
    }
}
