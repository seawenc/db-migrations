package me.seawenc.db.migration.helper;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



/**
 * 封装得最简单日志工具类
 */
public class Log {
    /**
     * 定义一个全局的记录器，通过LoggerFactory获取
     */
    private static final Logger LOG = LoggerFactory.getLogger(Log.class);

    /**
     * Info. info日志
     *
     * @param msg    the msg
     * @param params the params
     */
    public static void info(String msg, Object... params) {
        LOG.info(makeMsg(formatMsg(msg, params)));
    }

    public static void info(Boolean isPrint,String msg, Object... params) {
        if(isPrint) info(msg,params);
    }

    public static void condLevel(boolean isWarn,String msg, Object... params) {
        String msg1 = makeMsg(formatMsg(msg, params));
        Optionalx.ifThen(isWarn,()->LOG.warn(msg1));
        Optionalx.ifThen(!isWarn,()->LOG.info(msg1));
    }
    public static void condWarn(boolean isPrint,String msg, Object... params) {
        String msg1 = makeMsg(formatMsg(msg, params));
        Optionalx.ifThen(isPrint,()->LOG.warn(msg1));
    }
    public static void condInfo(boolean isPrint,String msg, Object... params) {
        String msg1 = makeMsg(formatMsg(msg, params));
        Optionalx.ifThen(isPrint,()->LOG.info(msg1));
    }

    /**
     * Warn. warn日志
     *
     * @param msg    the msg
     * @param params the params
     */
    public static void warn(String msg, Object... params) {
        LOG.warn(makeMsg(formatMsg(msg, params)));
    }
    public static void warn(Boolean isPrint,String msg, Object... params) {
        if(isPrint) warn(msg,params);
    }
    /**
     * Warn. warn错误日志
     *
     * @param msg       the msg
     * @param throwable the throwable
     */
    public static void warn(String msg, Throwable throwable) {
        LOG.warn(makeMsg(msg), throwable);
    }

    /**
     * Error. error日志
     *
     * @param msg    the msg
     * @param params the params
     */
    public static void error(String msg, Object... params) {
        LOG.error(makeMsg(formatMsg(msg, params)));
    }

    /**
     * Error. error日志
     *
     * @param msg       the msg
     * @param throwable the throwable
     */
    public static void error(String msg, Throwable throwable) {
        LOG.error(makeMsg(msg), throwable);
    }
    /**
     * Debug. debug日志
     *
     * @param msg       the msg
     * @param params the params
     */
    public static void debug(String msg, Object... params){
        LOG.debug(makeMsg(formatMsg(msg, params)));
    }

    /**
     * @Description: 拼成日志信息
     * @param msg msg
     * @return java.lang.String
     * @author: xyh
     * @Date: 2020年05月11日 15:00
     **/
    private static String makeMsg(String msg) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        // 默认在第4个
        StackTraceElement caller = elements[3];
        String[] nameSplit = caller.getClassName().split("\\.");
        String threadName = Thread.currentThread().getName();
        threadName=threadName.substring(threadName.length()-4);
        String name = nameSplit[nameSplit.length - 1];
        return threadName+ " [" + name + "." + caller.getMethodName() + ":"
                + caller.getLineNumber()  + findUserId() + "] " + msg;
    }

    /**
     * @Description: 找出当前线程userid
     * @return java.lang.String
     * @author: xyh
     * @Date: 2020年05月11日 15:03
     **/
    private static String findUserId() {
        // FIXME 找出当前线程userid
//		return "->anon";
        return "";
    }

    /**
     * @Description: 格式化
     * @param msg msg
     * @param params params
     * @return java.lang.String
     * @author: xyh
     * @Date: 2020年05月11日 15:04
     **/
    private static String formatMsg(String msg, Object[] params) {
        if(params==null|| params.length==0){
            return msg;
        }
        try {
            return  String.format(msg, params);
        } catch (Exception e) {
            return "格式化日志出错,可能入参中有特殊字符,msg:"+e.getMessage();
        }
    }
}
