package me.seawenc.db.migration.service.translator;

import me.seawenc.db.migration.bean.TableBean;
import me.seawenc.db.migration.bean.table.TableFieldBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 将逻辑模型翻译成 物理模型， 将物理模型翻译成逻辑模型
 */
public interface BaseTranslator {
    /**
     *  字段类型转换逻辑模型
     * @param tableFieldBean
     */
    void filedType2Logic(TableFieldBean tableFieldBean);

    /**
     * 将逻辑模型的字段类型转换成 物理模型
     * @param tableFieldBean
     */
    void filedType2Physical(TableFieldBean tableFieldBean);

    /**
     * 生成建表语句
     * @param table
     * @return
     */
    String buildCreateTableSql(TableBean table);

    default List<String> list(String... items) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, items);
        return list;
    }
}
