package me.seawenc.db.migration.service.translator.postgresql;

import me.seawenc.db.migration.bean.TableBean;
import me.seawenc.db.migration.bean.table.TableFieldBean;
import me.seawenc.db.migration.dbengine.FieldType;
import me.seawenc.db.migration.helper.Optionalx;
import me.seawenc.db.migration.service.translator.BaseTranslator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static me.seawenc.db.migration.dbengine.FieldType.*;

public class PostgresqlTranslator implements BaseTranslator {
    private final Map<FieldType, List<String>> MAPPING = new HashMap<>();

    public PostgresqlTranslator() {
        MAPPING.put(INTEGER, list("INT", "INTEGER", "SERIAL"));
        MAPPING.put(TINYINT, list("TINYINT","BOOLEAN"));
        MAPPING.put(MEDIUMINT, list("MEDIUMINT"));
        MAPPING.put(BIGINT, list("BIGINT", "BIGSERIAL"));
        MAPPING.put(SMALLINT, list("SMALLINT", "SMALLSERIAL"));
        MAPPING.put(DECIMAL, list("DECIMAL","MONEY"));
        MAPPING.put(NUMERIC, list("NUMERIC"));
        MAPPING.put(FLOAT, list("FLOAT"));
        MAPPING.put(REAL, list("REAL"));
        MAPPING.put(DOUBLE, list("DOUBLE", "DOUBLE PRECISION"));
        MAPPING.put(BIT, list("BIT"));
        MAPPING.put(CHAR, list("CHAR", "CHARACTER"));
        MAPPING.put(VARCHAR, list("VARCHAR", "CHARACTER VARYING", "CIDR", "INET", "MACADDR"));
        MAPPING.put(BINARY, list("BINARY"));
        MAPPING.put(VARBINARY, list("VARBINARY"));
        MAPPING.put(BLOB, list("BLOB"));
        MAPPING.put(LONGBLOB, list("BYTEA"));

        MAPPING.put(TEXT, list("TEXT","TINYTEXT","MEDIUMTEXT","LONGTEXT"));
        MAPPING.put(ENUM, list("ENUM"));
        MAPPING.put(SET, list("SET"));
        MAPPING.put(JSON, list("JSON", "JSONB"));
        MAPPING.put(DATE, list("DATE"));
        MAPPING.put(DATETIME, list("DATETIME"));
        MAPPING.put(TIMESTAMP, list("TIMESTAMP"));
        MAPPING.put(TIME, list("TIME", "INTERVAL"));
        MAPPING.put(YEAR, list("YEAR"));
        MAPPING.put(POLYGON, list("BOX", "CIRCLE"));
        MAPPING.put(LINESTRING, list("LINE", "LSEG","PATH"));

    }

    @Override
    public void filedType2Logic(TableFieldBean tableFieldBean) {
        FieldType logicType = MAPPING.entrySet().stream()
                .filter(entry -> entry.getValue().contains(tableFieldBean.getSrcType().toUpperCase()))
                .findAny()
                .orElseThrow(() -> new RuntimeException("未知的字段类型：" + tableFieldBean.getSrcType()))
                .getKey();
        tableFieldBean.setLogicType(logicType);
    }

    @Override
    public void filedType2Physical(TableFieldBean tableFieldBean) {
        // 因为逻辑模型以mysql为基础，因此不需要转换
        return ;
    }

    @Override
    public String buildCreateTableSql(TableBean table) {
        // 生成基本结构
        StringBuilder sql = new StringBuilder(String.format("create table %s (",table.getTableName()));
        for (int i = 0; i < table.getFields().size(); i++) {
            TableFieldBean field = table.getFields().get(i);
            String type= Optionalx.isPresent(field.getLength())?String.format("%s(%s)",field.getSrcType(),field.getLength()):field.getSrcType();
            String allowNull = "N".equals(field.getAllowNull()) ? "" : "not null";
            String defaultVal = Optionalx.isPresent(field.getDefaultValue())?String.format("default %s",field.getDefaultValue()):"";
            String splitChar = i == table.getFields().size() - 1 ? "" : ",";
            // 格式例：varchar_column VARCHAR(255) NOT NULL DEFAULT 'default text',
            sql.append(String.format("%s %s %s %s %s\n",field.getName(),type, allowNull,defaultVal, splitChar));
        }
        sql.append(");\n\n");

        // 设置主键
        String idFields = table.getFields().stream().filter(f -> f.getPriKey() != null).map(f -> f.getName()).collect(Collectors.joining(","));
        if(Optionalx.isPresent(idFields)){
            sql.append(String.format("alter table %s add primary key (%s);\n\n",table.getTableName(),idFields));
        }

        // 生成注释
        for (TableFieldBean field: table.getFields()) {
            sql.append(String.format("comment on column %s.%s is '%s';\n",table.getTableName(),field.getName(),field.getComment()));
        }

        return sql.toString();
    }

}
