package me.seawenc.db.migration.service;

import java.util.Properties;
import me.seawenc.db.migration.bean.DatabaseBean;
import me.seawenc.db.migration.bean.FileConfig;
import me.seawenc.db.migration.dbengine.DbType;
import me.seawenc.db.migration.dbengine.IEngineService;
import me.seawenc.db.migration.helper.FileHelper;
import static me.seawenc.db.migration.dbengine.DbType.findTypeByDriver;

public class TableService {
    IEngineService dbEngine;
    DbType dsType;

    public TableService(String confKeyPrefix) throws Exception {
        FileConfig config = createFileParameters(confKeyPrefix);
        dbEngine = DbType.findDbEngine(config);
        dsType = findTypeByDriver(config.getJdbcDriver());
    }


    public String findVersion() throws Exception {
        return dbEngine.findVersion();
    }

    public DatabaseBean buildDatabaseStrut() throws Exception {
        DatabaseBean database=new DatabaseBean();
        database.setVersion(dbEngine.findVersion());
        database.setDbType(dsType);
        return database;
    }

    private static FileConfig createFileParameters(String filePath) throws Exception {
        Properties props = FileHelper.readExtResFile(filePath);
        return FileConfig.getInstance(props);
    }

}
