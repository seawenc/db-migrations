package me.seawenc.db.migration.dbengine.impl.engines.bean;

public class TableNameBean {
    /**
     * 表名
     */
    private String tableName;
    /**
     * 注释
     */
    private String tableNotes;

    public TableNameBean(){}

    public TableNameBean(String tableName, String tableNotes) {
        this.tableName = tableName;
        this.tableNotes = tableNotes;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableNotes() {
        return tableNotes;
    }

    public void setTableNotes(String tableNotes) {
        this.tableNotes = tableNotes;
    }
}
