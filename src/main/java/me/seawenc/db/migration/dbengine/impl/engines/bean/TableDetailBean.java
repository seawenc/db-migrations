package me.seawenc.db.migration.dbengine.impl.engines.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 表明细信息
 */
public class TableDetailBean {
    /**
     * 表名
     */
    private String tableName = "";

    /**
     * 数据库列
     */
    private List<TableColumnBean> tableColumns = new ArrayList<>();
    /**
     * 统计以上信息耗时ms
     */
    private long execCost=0l;

    public TableDetailBean() {
    }

    public TableDetailBean(String tableName, List<JSONObject> ret) {
        //先去重，因为hive-spark sql返回有重复字段
        Map<Object, TableColumnBean> map = ret.stream().map(row -> new TableColumnBean(row))
                .collect(Collectors.toMap(v -> v.getColumnName(), v -> v, (v1, v2) -> v1));
        tableColumns.addAll(map.values());
        this.tableName = tableName;
    }
    public TableDetailBean(String tableName) {
        this.tableName = tableName;
    }
    public void addCol(TableColumnBean columnBean){
        this.tableColumns.add(columnBean);
    }
    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<TableColumnBean> getTableColumns() {
        return tableColumns;
    }

    public void setTableColumns(List<TableColumnBean> tableColumns) {
        this.tableColumns = tableColumns;
    }

    public long getExecCost() {
        return execCost;
    }

    public void setExecCost(long execCost) {
        this.execCost = execCost;
    }
}
