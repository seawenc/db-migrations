package me.seawenc.db.migration.dbengine;

/**
 * 字段类型(基于OceanBase-Mysql租户的基础字段类型)
 */
public enum FieldType {
    /* 数值型数据类型 */
    BIT,
    TINYINT,
    BOOLEAN,
    SMALLINT,
    MEDIUMINT,
    INTEGER,
    BIGINT,
    DECIMAL,
    NUMERIC,
    FLOAT,
    REAL,
    DOUBLE,

    /* 字符串数据类型 */
    CHAR,
    VARCHAR,
    BINARY,
    VARBINARY,
    TINYBLOB,
    BLOB,
    MEDIUMBLOB,
    LONGBLOB,
    TINYTEXT,
    TEXT,
    MEDIUMTEXT,
    LONGTEXT,
    ENUM, // OceanBase 数据库 MySQL 租户 V2.2.0+
    SET, // OceanBase 数据库 MySQL 租户 V2.2.0+
    JSON, // OceanBase 数据库 MySQL 租户 V3.2.2+

    /* 日期和时间数据类型 */
    DATE,
    DATETIME,
    TIMESTAMP,
    TIME,
    YEAR,

    /* 空间数据类型 */
    GEOMETRY,
    LINESTRING,
    POLYGON,
    POINT,
    GEOMETRYCOLLECTION,
    MULTILINESTRING,
    MULTIPOLYGON,
    MULTIPOINT
}
