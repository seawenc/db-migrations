package me.seawenc.db.migration.dbengine;

import me.seawenc.db.migration.bean.FileConfig;
import me.seawenc.db.migration.dbengine.impl.engines.*;

import java.lang.reflect.Constructor;

/**
 * 数据源类型
 */
public enum DbType {
    // 将mysql做为数据转换的中间类型， 例如，将oracle转为postgres,则先将oracle转为mysql类型,再将mysql类型转为postgres
    MYSQL(EngineServiceMsyql.class),
    POSTGRESQL(EngineServicePostgresql.class),
//    HIVE(EngineServiceHive.class),

    ORACLE(EngineServiceOracle.class);
//    PRESTO(EngineServicePresto.class);
    private Class<? extends IEngineService> engineCLass;
    DbType(Class<? extends IEngineService> engineService){
        this.engineCLass =engineService;
    }

    public String lname(){
        return this.name().toLowerCase();
    }

    public static IEngineService findDbEngine(FileConfig fileConfig) throws Exception {
        DbType dsType = findTypeByDriver(fileConfig.getJdbcDriver());
        return dsType.getEngineService(fileConfig);
    }

    private IEngineService getEngineService(FileConfig parameters) throws Exception {
        Constructor<? extends IEngineService> constructor = this.engineCLass.getConstructor(FileConfig.class);
        IEngineService engineService = constructor.newInstance(parameters);
        return engineService;
    }

    /**
     * 如果驱动串中包含，关键字，则直接返回
     * @param dbDriver
     * @return
     */
    public static DbType findTypeByDriver(String dbDriver){
        for (DbType dsType: DbType.values()){
            if(dbDriver.contains(dsType.lname())){
                return dsType;
            }
        }
        return DbType.valueOf(dbDriver.toUpperCase());
    }
}
