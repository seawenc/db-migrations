package me.seawenc.db.migration.dbengine.impl.engines.constant;

/**
 * 表字段常量
 */
public enum ConstantFieldName {
    //数量：count(*) as NUM
    VERSION,
    // 序号
    ATTNUM,
    // 列名
    COLUMN_NAME,
    // 数据类型
    DATA_TYPE,
    // 是否允许为空
    ALLOW_NULL,
    // 表名
    TAB_NAME,
    // 字段长度
    FIELD_LENGTH,
    // 如果是数据类型，则此值为精度
    NUM_SCALE,
    // 注释
    COMMENT;

    @Override
    public String toString(){
        return this.name().toLowerCase();
    }
}
