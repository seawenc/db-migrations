package me.seawenc.db.migration.dbengine.impl.engines;


import me.seawenc.db.migration.dbengine.DbType;
import me.seawenc.db.migration.bean.FileConfig;

/**
 * 数据库执行引擎-oracle实现
 * Servicename的格式为：dbengine_{com.dgp.common.enums.DsType}
 */
public class EngineServiceOracle extends EngineServiceBase {
    /** ORACLE 版本字段名 */
    private static final String BANNER = "BANNER";

    public EngineServiceOracle(FileConfig conf) throws Exception {
        super(conf);
    }

    @Override
    protected DbType getDsType() {
        return DbType.ORACLE;
    }


    @Override
    public String findVersion() throws Exception {
        String sql = getVersionSql(getDsType().lname());
        return execSqlTemplete(sql).get(0).getString(BANNER);
    }
}
