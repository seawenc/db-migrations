package me.seawenc.db.migration.dbengine.impl.engines.bean;

import com.alibaba.fastjson.JSONObject;
import me.seawenc.db.migration.dbengine.impl.engines.constant.ConstantFieldName;
import me.seawenc.db.migration.helper.Optionalx;

/**
 * 表字段
 */
public class TableColumnBean {
    /**
     * 列名
     */
    private String columnName;
    /**
     * 列类型
     */
    private String dataType;
    /**
     * 不为空
     */
    private String notnull="N";
    /**
     * 列注释
     */
    private String comment;
    /**
     * 序号
     */
    private String attnum;

    public TableColumnBean(){
    }

    public TableColumnBean(JSONObject row) {
        this.columnName = row.getString(ConstantFieldName.COLUMN_NAME.toString());
        this.dataType = row.getString(ConstantFieldName.DATA_TYPE.toString());
        this.notnull = row.getString(ConstantFieldName.ALLOW_NULL.toString());
        this.comment = row.getString(ConstantFieldName.COMMENT.toString());
        this.attnum = row.getString(ConstantFieldName.ATTNUM.toString());
        //格式化输出
        this.dataType= Optionalx.ifPresentAndRet(this.dataType,(v)-> v.replace("character varying","varchar"),"");
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }
}
