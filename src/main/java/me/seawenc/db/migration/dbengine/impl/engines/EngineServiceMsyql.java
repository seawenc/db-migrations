package me.seawenc.db.migration.dbengine.impl.engines;

import me.seawenc.db.migration.dbengine.DbType;
import me.seawenc.db.migration.bean.FileConfig;
import me.seawenc.db.migration.service.translator.BaseTranslator;
import me.seawenc.db.migration.service.translator.mysql.MysqlTranslator;

/**
 * 数据库执行引擎-mysql实现
 * Servicename的格式为：dbengine_{com.dgp.common.enums.DsType}
 */
public class EngineServiceMsyql extends EngineServiceBase {
    public EngineServiceMsyql(FileConfig conf) throws Exception {
        super(conf);
    }

    @Override
    protected DbType getDsType() {
        return DbType.MYSQL;
    }

    @Override
    public BaseTranslator getTranslator() {
        return new MysqlTranslator();
    }
}
