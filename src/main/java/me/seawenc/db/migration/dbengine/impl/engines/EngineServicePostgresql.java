package me.seawenc.db.migration.dbengine.impl.engines;

import me.seawenc.db.migration.dbengine.DbType;
import me.seawenc.db.migration.bean.FileConfig;
import me.seawenc.db.migration.service.translator.BaseTranslator;
import me.seawenc.db.migration.service.translator.postgresql.PostgresqlTranslator;

/**
 * 数据库执行引擎-hive实现
 */
public class EngineServicePostgresql extends EngineServiceBase {

    public EngineServicePostgresql(FileConfig conf) throws Exception {
        super(conf);
    }

    @Override
    protected DbType getDsType() {
        return DbType.POSTGRESQL;
    }


    @Override
    public BaseTranslator getTranslator() {
        return new PostgresqlTranslator();
    }
}
