package me.seawenc.db.migration.dbengine;

import com.alibaba.fastjson.JSONObject;
import me.seawenc.db.migration.bean.TableBean;
import me.seawenc.db.migration.bean.table.TableIndexBean;
import me.seawenc.db.migration.bean.table.TablePartitionBean;
import me.seawenc.db.migration.bean.table.TableRefBean;
import me.seawenc.db.migration.dbengine.impl.engines.bean.TableNameBean;

import java.util.List;
import me.seawenc.db.migration.service.translator.BaseTranslator;

/**
 * 数据库引擎
 */
public interface IEngineService {

    /**
     * 获得数据库信息，用于测试数据连通性等
     * @return
     */
    String findVersion() throws Exception;

    /**
     * 获得所有的数据库表
     * @return
     * @throws Exception
     */
    List<TableNameBean> findAllTables() throws Exception;

    /**
     * 获得表明细
     */
    TableBean findTableDetail(String tableName) throws Exception;

    /**
     * 获得表明细
     */
    TableBean findTableInfo(String tableName) throws Exception;

    /**
     * 判断是否有数据
     * @param tableName
     * @return
     * @throws Exception
     */
    boolean hasData(String tableName) throws Exception;

    /**
     * 获得表的外键信息
     * @param tableName
     * @return
     * @throws Exception
     */
    List<TableRefBean> findTableRefs(String tableName) throws Exception;

    List<TableIndexBean> findTableIndexs(String tableName) throws Exception;

    List<TablePartitionBean> findTablePartitions(String tableName) throws Exception;

    BaseTranslator getTranslator();


    List<JSONObject> execSelectSql(String sql) throws Exception;

    void closeDbCollection()throws Exception;

}
