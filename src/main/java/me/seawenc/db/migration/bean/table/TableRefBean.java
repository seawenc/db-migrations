package me.seawenc.db.migration.bean.table;

import com.alibaba.fastjson.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class TableRefBean {
    private String name;
    // 当前表外键关联字段
    private TableFieldBean field;
    // 目标表字段
    private TableFieldBean refField;
    // 更新策略以mysql为例
    private String  updateRule;
    private String  deleteRule;

    public static List<TableRefBean> createInstance(List<JSONObject> jsonObjects) {
        List<TableRefBean> refs=new ArrayList<>();
        for (JSONObject jsonObject : jsonObjects){
            TableRefBean ref=new TableRefBean();
            ref.name= jsonObject.getString("ref_name");
            ref.field = new TableFieldBean(jsonObject.getString("tab_name"),jsonObject.getString("column_name"));
            ref.refField = new TableFieldBean(jsonObject.getString("ref_table_name"),jsonObject.getString("ref_column_name"));
            ref.updateRule= jsonObject.getString("update_rule");
            ref.deleteRule= jsonObject.getString("delete_rule");
            refs.add(ref);
        }
        return refs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TableFieldBean getField() {
        return field;
    }

    public void setField(TableFieldBean field) {
        this.field = field;
    }

    public TableFieldBean getRefField() {
        return refField;
    }

    public void setRefField(TableFieldBean refField) {
        this.refField = refField;
    }

    public String getUpdateRule() {
        return updateRule;
    }

    public void setUpdateRule(String updateRule) {
        this.updateRule = updateRule;
    }

    public String getDeleteRule() {
        return deleteRule;
    }

    public void setDeleteRule(String deleteRule) {
        this.deleteRule = deleteRule;
    }
}
