package me.seawenc.db.migration.bean.table;

import com.alibaba.fastjson.JSONObject;
import me.seawenc.db.migration.dbengine.FieldType;
import me.seawenc.db.migration.dbengine.impl.engines.constant.ConstantFieldName;
import me.seawenc.db.migration.helper.Optionalx;

public class TableFieldBean {
    private String tableName;
    // 排序
    private Integer attnum;
    private String name;

    // 是否主键
    private String priKey;
    // 对应数据库中的原始类型，
    private String srcType;

    // 逻辑类型，以flink为标准类型，转换过后的公共类型
    private FieldType logicType;

    // 长度，不能为数字，因为有的类型是带精度的，如：numeric(10,2),则此处为：10,2
    private String  length;
    // 是否允许为空
    private String allowNull;
    private String defaultValue;
    private String  comment;
    // 额外信息,例如mysql的auto_increment
    private String extra;

    public TableFieldBean(){}

    public TableFieldBean(JSONObject row) {
        this.name = row.getString(ConstantFieldName.COLUMN_NAME.toString());
        this.srcType = row.getString(ConstantFieldName.DATA_TYPE.toString());
        this.allowNull = row.getString(ConstantFieldName.ALLOW_NULL.toString());
        this.comment = row.getString(ConstantFieldName.COMMENT.toString());
        this.attnum = Integer.parseInt(Optionalx.ifPresentAndRet(row.getString(ConstantFieldName.ATTNUM.toString()),(num)->num,"0"));
        this.length=row.getString(ConstantFieldName.FIELD_LENGTH.toString());
        // 如果有精度，则加上精度
        if(Optionalx.isPresent(this.length)&&Optionalx.isPresent(row.getString(ConstantFieldName.NUM_SCALE.toString()))){
            this.length+=","+row.getString(ConstantFieldName.NUM_SCALE.toString());
        }
        //格式化输出
        this.srcType = Optionalx.ifPresentAndRet(this.srcType,(v)-> v.replace("character varying","varchar"),"");
        // 如果类型中带了长度，则从类型中找出length,例decimal(10,2)
        if(this.srcType.contains("(") && this.srcType.endsWith(")")){
            this.length=this.srcType.substring(this.srcType.indexOf("(")+1,this.srcType.indexOf(")"));
            this.srcType =this.srcType.substring(0,this.srcType.indexOf("("));
        }
        this.extra=row.getString("extra");
    }

    public TableFieldBean(String tableName, String name) {
        this.tableName = tableName;
        this.name = name;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getAttnum() {
        return attnum;
    }

    public void setAttnum(int attnum) {
        this.attnum = attnum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriKey() {
        return priKey;
    }

    public void setPriKey(String priKey) {
        this.priKey = priKey;
    }

    public String getSrcType() {
        return srcType;
    }

    public void setSrcType(String srcType) {
        this.srcType = srcType;
    }

    public FieldType getLogicType() {
        return logicType;
    }

    public void setLogicType(FieldType logicType) {
        this.logicType = logicType;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getAllowNull() {
        return allowNull;
    }

    public void setAllowNull(String allowNull) {
        this.allowNull = allowNull;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setAttnum(Integer attnum) {
        this.attnum = attnum;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
