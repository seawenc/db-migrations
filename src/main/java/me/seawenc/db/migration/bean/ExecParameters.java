package me.seawenc.db.migration.bean;

import com.alibaba.fastjson.JSON;
import me.seawenc.db.migration.helper.FileHelper;
import me.seawenc.db.migration.helper.Log;
import me.seawenc.db.migration.helper.Optionalx;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * 执行类的参数
 */
public class ExecParameters {


    // 是否是显示帮助
    private boolean isHelper=false;
    // 配置文件路径
    private String configPath="conf/conf.yaml";

    /**
     * 无参构造
     */
    public ExecParameters() {
    }


    /**
     * 数据初始化
     * @param args args
     * @return ExecParameters
     */
    public static ExecParameters getInstance(String[] args) {
        ExecParameters parameters = new ExecParameters();
        parameters.assembleParams(args);
        parameters.assertParams();
        Log.info("当前参数：%s", JSON.toJSONString(parameters));
        return parameters;
    }

    private void assertParams() {
        if(isHelper){
            printHelp();
        }
        if(!configPath.endsWith("yaml")){
            throw new IllegalArgumentException("配置文件只能是yaml文件");
        }
    }

    public static void printHelp() {
        System.out.println(FileHelper.readResFile("/help.txt"));
        System.exit(0);
    }

    /**
     * 组装传入参数
     * @param args
     */
    private void assembleParams(String... args) {
        // 过滤掉空参数（多输入了空格导致）
        args=Arrays.stream(args).filter(arg-> Optionalx.isPresent(arg)).collect(Collectors.toList()).toArray(new String[]{});

        //组装参数
        for (int i=0;i< args.length;i++){
            String arg=args[i]+"";
            if(" --config".equals(arg) || " -c".equals(arg)){
                configPath=args[++i];
            }
        }
    }
    public boolean isHelper() {
        return isHelper;
    }

    public String getConfigPath() {
        return configPath;
    }

}
