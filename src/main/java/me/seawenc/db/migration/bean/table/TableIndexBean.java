package me.seawenc.db.migration.bean.table;


import com.alibaba.fastjson.JSONObject;

public class TableIndexBean {

    private String name;

    private String indexType;
    //字段，多个字段以，隔开
    private String fields;
    // 是否是惟一索引，Y是，N否
    private String uniqueIndex;

    public TableIndexBean() {

    }

    public TableIndexBean(JSONObject indexJson) {
        this.name=indexJson.getString("index_name");
        this.indexType=indexJson.getString("index_type");
        this.fields=indexJson.getString("index_columns");
        this.uniqueIndex=indexJson.getString("unique_index");
    }

    public String getUniqueIndex() {
        return uniqueIndex;
    }

    public void setUniqueIndex(String uniqueIndex) {
        this.uniqueIndex = uniqueIndex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndexType() {
        return indexType;
    }

    public void setIndexType(String indexType) {
        this.indexType = indexType;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }
}
