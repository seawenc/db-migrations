package me.seawenc.db.migration.bean;

import java.util.List;
import me.seawenc.db.migration.dbengine.DbType;

public class DatabaseBean {

    private DbType dbType;

    private String version;

    private List<TableBean> dbTables;

    // 本逻辑库来源的 物理库信息，只有逻辑库才有此值
    private DatabaseBean physicalDatabase;

    public DbType getDbType() {
        return dbType;
    }

    public void setDbType(DbType dbType) {
        this.dbType = dbType;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<TableBean> getDbTables() {
        return dbTables;
    }

    public void setDbTables(List<TableBean> dbTables) {
        this.dbTables = dbTables;
    }

    public DatabaseBean getPhysicalDatabase() {
        return physicalDatabase;
    }

    public void setPhysicalDatabase(DatabaseBean physicalDatabase) {
        this.physicalDatabase = physicalDatabase;
    }
}
