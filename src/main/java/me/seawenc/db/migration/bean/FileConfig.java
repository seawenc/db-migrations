package me.seawenc.db.migration.bean;


import me.seawenc.db.migration.helper.Optionalx;

import java.util.Properties;

/**
 * 数据源配置
 */
public class FileConfig {
    private String jdbcDriver;
    private String jdbcUser;
    private String jdbcPwd;
    private String jdbcUrl;


    public static FileConfig getInstance(Properties props) {
        FileConfig parameters=new FileConfig();
        parameters.jdbcDriver=props.getProperty("driver");
        parameters.jdbcUser=props.getProperty("user");
        parameters.jdbcPwd =props.getProperty("pwd");
        parameters.jdbcUrl =props.getProperty("url");

        parameters.assertParamValid();
        return parameters;
    }

    private void assertParamValid(){
        if(Optionalx.isNotPresent(jdbcDriver)||
                Optionalx.isNotPresent(jdbcUser)||
                Optionalx.isNotPresent(jdbcPwd)||
                Optionalx.isNotPresent(jdbcUrl)){
            throw new IllegalArgumentException("配置文件中jdbc的4个参数全部必传!");
        }
    }

    public String getJdbcUser() {
        return jdbcUser;
    }

    public void setJdbcUser(String jdbcUser) {
        this.jdbcUser = jdbcUser;
    }

    public String getJdbcPwd() {
        return jdbcPwd;
    }

    public void setJdbcPwd(String jdbcPwd) {
        this.jdbcPwd = jdbcPwd;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getJdbcDriver() {
        return jdbcDriver;
    }

    public void setJdbcDriver(String jdbcDriver) {
        this.jdbcDriver = jdbcDriver;
    }
}
