select * from (
    select
           ORDINAL_POSITION attnum,
           COLUMN_NAME column_name,
           column_type data_type,
           Column_default default_value,
           Numeric_scale num_scale,
           case when NUMERIC_PRECISION is not null then NUMERIC_PRECISION else character_maximum_length end filed_length,
           case when column_key='PRI' then 'Y' else 'N' end pri_key,
           case when IS_NULLABLE then 'Y' else 'N' end as allow_null,
           case when COLUMN_COMMENT is null then '' else replace(replace(replace(COLUMN_COMMENT,char(10),'&'),char(13),'&'),char(124),' & ') end as comment,
           extra
    from information_schema.columns
    where table_schema = schema()
    and table_name = '_table_name_'
) t
