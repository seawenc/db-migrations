-- 获得表外键信息
SELECT
    ku.TABLE_NAME tab_name,
    ku.COLUMN_NAME column_name,
    ku.CONSTRAINT_NAME ref_name ,
    ku.REFERENCED_TABLE_NAME ref_table_name,
    ku.REFERENCED_COLUMN_NAME ref_column_name,
    rc.UPDATE_RULE update_rule,
    rc.DELETE_RULE delete_rule
FROM
    INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS ku
JOIN
   INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS AS rc
ON
   ku.CONSTRAINT_NAME = rc.CONSTRAINT_NAME
WHERE
   ku.TABLE_SCHEMA = schema()
    AND ku.TABLE_NAME = '_table_name_'
    AND ku.REFERENCED_TABLE_NAME IS NOT NULL;