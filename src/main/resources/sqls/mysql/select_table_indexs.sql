SELECT
    TABLE_NAME table_name,
    INDEX_NAME index_name,
    INDEX_TYPE index_type,
    max(case when NON_UNIQUE=0 then 'Y' else 'N' end) unique_index,
    GROUP_CONCAT(COLUMN_NAME ORDER BY SEQ_IN_INDEX) index_columns
FROM
    INFORMATION_SCHEMA.STATISTICS
WHERE
    TABLE_SCHEMA = SCHEMA()
    and TABLE_NAME= '_table_name_'
GROUP BY
    TABLE_NAME, INDEX_NAME, INDEX_TYPE;