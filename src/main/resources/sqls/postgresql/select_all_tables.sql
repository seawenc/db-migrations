--查询数据库中所有的表
select DISTINCT relname as tab_name ,
            case when cast(obj_description(relfilenode,'pg_class') as varchar) is null then relname else replace(replace(cast(obj_description(relfilenode,'pg_class') as varchar),chr(10),'&'),chr(13),'&') end as comment
            from pg_class c where  relkind = 'r' and relname not like 'pg_%' and relname not like 'sql_%' and relname not like 'edb%' and relname not like 'plsql_%' and relchecks=0 and relname!='dual'
                              and relname in(select tablename from pg_tables)
                              and c.oid not in (select inhrelid from pg_inherits)
                              and relnamespace=(select oid from pg_catalog.pg_namespace where nspname=current_schema())
order by relname