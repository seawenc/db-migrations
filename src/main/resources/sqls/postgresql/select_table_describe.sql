select
ordinal_position attnum,
c.column_name, data_type,
  case when character_maximum_length is not null then character_maximum_length when numeric_precision is not null then numeric_precision end filed_length,
  numeric_scale num_scale,
  is_nullable allow_null,
  case when pkf is not null then 'Y' else 'N' end pri_key
FROM information_schema.columns c left join (
    SELECT a.attname AS column_name FROM pg_index i JOIN pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
    WHERE i.indrelid = '_table_name_'::regclass AND i.indisprimary
) pkf on pkf.column_name=c.column_name
WHERE table_schema = "current_schema"()
AND table_name = '_table_name_';