select tablename table_name,
   indexname indexname,
   case when indexdef like '%USING hash%' then 'hash'
        when indexdef like '%USING gist%' then 'gist'
        when indexdef like '%USING gin%' then 'gin'
        when indexdef like '%USING spgist%' then 'spgist'
        when indexdef like '%USING btree%' then ' btree' end index_type,
    case when indexdef like '%UNIQUE INDEX%' then 'Y' else 'N' end unique_index,
    substring( indexdef FROM '\(([^)]+)\)') index_columns
FROM
    pg_indexes
WHERE
    tablename = '_table_name_' and schemaname=current_schema();