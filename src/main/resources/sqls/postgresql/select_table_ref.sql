SELECT
    tc.constraint_name AS ref_name,
    tc.table_name AS tab_name,
    kcu.column_name AS column_name,
    ccu.table_name AS ref_table_name,
    ccu.column_name AS ref_column_name,
    rc.update_rule AS update_rule,
    rc.delete_rule AS delete_rule
FROM
    information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name
    JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name
    LEFT JOIN information_schema.referential_constraints AS rc ON rc.constraint_name = tc.constraint_name
WHERE tc.table_name = '_table_name_' and (rc.update_rule is not null or rc.delete_rule is not null);