--查询表分区
SELECT
    child.relname AS tab_name
FROM
    pg_inherits JOIN pg_class parent
    ON pg_inherits.inhparent = parent.oid JOIN pg_class child
    ON pg_inherits.inhrelid = child.oid JOIN pg_namespace nmsp_parent
    ON nmsp_parent.oid = parent.relnamespace JOIN pg_namespace nmsp_child ON nmsp_child.oid = child.relnamespace
WHERE
        parent.relname = '_table_name_';