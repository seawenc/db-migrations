--查询表结构
select DISTINCT t.attnum,t.column_name as column_name,t.data_type as data_type,t.attrnotnull as attr_notnull,t.comment as comment from (
  SELECT a.attnum,a.attname                             AS column_name,
         format_type(a.atttypid, a.atttypmod)  AS data_type,
         case when a.attnotnull then 'Y' else 'N' end                        as attrnotnull,
         case when col_description(a.attrelid, a.attnum) is null then '' else replace(replace(replace(col_description(a.attrelid, a.attnum),chr(10),'&'),chr(13),'&'),chr(124),' & ') end as comment
  FROM pg_class AS c, pg_attribute AS a
  WHERE c.relname = '_table_name_'
    and a.attname not like '%pg.dropped%'
    and relnamespace=(select oid from pg_catalog.pg_namespace where nspname="current_schema"())
    AND a.attrelid = c.oid AND a.attnum > 0
  order by a.attnum asc
) t order by t.attnum;