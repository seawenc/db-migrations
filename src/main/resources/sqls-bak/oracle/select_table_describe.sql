select DISTINCT
    utc.COLUMN_ID as "attnum",
    utc.COLUMN_NAME as "column_name",
    utc.NULLABLE as "attr_notnull",
    ucc.COMMENTS as "comment"
from USER_TAB_COLUMNS utc, USER_COL_COMMENTS ucc
where
      utc.COLUMN_NAME = ucc.COLUMN_NAME
  and
      utc.table_name = '_table_name_'
