select sum(table_set.BYTES) as total_size,
       sum(index_set.BYTES) as indexs_size
from (select * from USER_SEGMENTS where SEGMENT_NAME = '_table_name_' and SEGMENT_TYPE = 'TABLE') table_set,
     (select * from USER_SEGMENTS where SEGMENT_NAME like '%_table_name_%' and SEGMENT_TYPE like '%INDEX%') index_set