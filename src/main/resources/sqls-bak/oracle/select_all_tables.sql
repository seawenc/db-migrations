select distinct
                u_table.table_name "tab_name",
                u_comment.comments "comment"
from
     USER_TABLES u_table,USER_TAB_COMMENTS u_comment
where
      u_table.TABLE_NAME = u_comment.TABLE_NAME
order by
         u_table.table_name