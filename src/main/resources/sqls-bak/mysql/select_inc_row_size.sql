-- 如果不多包一层，字段别名将不生效
select * from (
    select
        count(1) as num
    from
        _table_name_
    where
        _inc_field_ between FROM_UNIXTIME(_being_timestamp_) and FROM_UNIXTIME(_end_timestamp_)
) t