-- 如果不多包一层，字段别名将不生效
select * from (
    select
        sum(data_length) as total_size,
        sum(index_length) as indexs_size
    from
        information_schema.tables
    where
        table_schema = schema()
        and TABLE_NAME = '_table_name_'
) t