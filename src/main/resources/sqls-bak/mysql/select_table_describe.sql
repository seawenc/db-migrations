select * from (
    select
           ORDINAL_POSITION as attnum,
           COLUMN_NAME as column_name,
           DATA_TYPE as data_type,
           case when IS_NULLABLE then 'Y' else 'N' end as attrnotnull,
           case when COLUMN_COMMENT is null then '' else replace(replace(replace(COLUMN_COMMENT,char(10),'&'),char(13),'&'),char(124),' & ') end as comment
    from information_schema.columns
    where table_schema = schema()
    and table_name = '_table_name_'
) t
