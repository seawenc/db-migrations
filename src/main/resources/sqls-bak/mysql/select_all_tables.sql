-- 如果不多包一层，字段别名将不生效
select * from (
    select
        distinct table_name tab_name,TABLE_COMMENT comment
    from
        information_schema.tables
    where
        table_schema=schema()
        and table_type='base table'
    order by table_name
) t
