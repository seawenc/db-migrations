-- 如果不多包一层，字段别名将不生效
select * from (
    select
        sum(data_length+index_length) as total_size
    from
        information_schema.tables
    where
        table_schema = schema()
) t